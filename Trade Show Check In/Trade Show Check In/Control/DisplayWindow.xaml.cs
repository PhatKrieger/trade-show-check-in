﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Trade_Show_Check_In.Class;

namespace Trade_Show_Check_In.Control
{
    /// <summary>
    /// Interaction logic for DisplayWindow.xaml
    /// </summary>
    public partial class DisplayWindow : Window
    {
        #region Field
        private List<Contact> _entries;
        #endregion

        public DisplayWindow(List<Contact> entries)
        {
            InitializeComponent();
            _entries = entries;
        }

        private void ContactButton_Click(object sender, RoutedEventArgs e)
        {
            ContactWindow cw = new ContactWindow(_entries);
            cw.ShowDialog();
        }
    }
}