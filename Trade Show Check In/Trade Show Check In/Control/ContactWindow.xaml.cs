﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Trade_Show_Check_In.Class;

namespace Trade_Show_Check_In.Control
{
    /// <summary>
    /// Interaction logic for ContactWindow.xaml
    /// </summary>
    public partial class ContactWindow : Window
    {
        private List<Contact> _contacts;

        public ContactWindow(List<Contact> entries)
        {
            InitializeComponent();
            _contacts = entries;
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            if (!IsValid())
            {
                MessageBox.Show("All fields are required");
                return;
            }

            Contact entry = new Contact
            {
                Name = NameTb.Text.Trim(),
                Email = EmailTb.Text.Trim(),
                FirstLine = FirstLineTb.Text.Trim(),
                SecondLine = SecondLineTb.Text.Trim(),
                City = CityTb.Text.Trim(),
                State = StateTb.Text.Trim(),
                Zip = ZipTb.Text.Trim(),
                Note = NoteTb.Text.Trim(),
            };

            _contacts.Add(entry);
            Close();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        #region validation
        private void TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            var tb = (TextBox)sender;
            if (tb == null) return; // dont know why this would happen but better safe

            if (String.IsNullOrWhiteSpace(tb.Text))
            {
                tb.BorderBrush = new SolidColorBrush(Colors.Red);
                tb.ToolTip = "This field is required";
            }
            else
            {
                tb.BorderBrush = new SolidColorBrush(Colors.LightBlue);
            }
        }

        private bool IsValid()
        {
            //if any of these is just white space, it will return false
            return !(string.IsNullOrWhiteSpace(NameTb.Text) || string.IsNullOrWhiteSpace(EmailTb.Text) ||
                string.IsNullOrWhiteSpace(FirstLineTb.Text) || string.IsNullOrWhiteSpace(CityTb.Text) ||
                string.IsNullOrWhiteSpace(StateTb.Text) || string.IsNullOrWhiteSpace(ZipTb.Text));
        }
        #endregion
    }
}