﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Trade_Show_Check_In.Class;
using Trade_Show_Check_In.Control;

namespace Trade_Show_Check_In
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Field
        private List<Contact> _entries;
        Random r = new Random();
        #endregion

        public MainWindow()
        {
            InitializeComponent();
            if (_entries == null) _entries = new List<Contact>();
        }

        private void ContactButton_Click(object sender, RoutedEventArgs e)
        {
            ContactWindow cw = new ContactWindow(_entries);
            cw.ShowDialog();
        }

        private void WinnerButton_Click(object sender, RoutedEventArgs e)
        {
            if (_entries.Count < 1) return;

            Contact winner = _entries[r.Next(0, _entries.Count)];
            WinnerWindow ww = new WinnerWindow(winner);
            ww.ShowDialog();
        }

        private void ExportTextButton_Click(object sender, RoutedEventArgs e)
        {
            var sfd = new SaveFileDialog();
            sfd.Filter = "txt files (*.txt)|*.txt";
            sfd.FilterIndex = 1;
            
            if (sfd.ShowDialog() == true)
            {
                string data = string.Empty;
                foreach (var entry in _entries)
                {
                    data += entry.TextFriendly() + Environment.NewLine + Environment.NewLine;
                }

                File.WriteAllText(sfd.FileName, data);
            }
        }

        private void DisplayButton_Click(object sender, RoutedEventArgs e)
        {
            DisplayWindow dw = new DisplayWindow(_entries);
            dw.ShowDialog();
        }
    }
}