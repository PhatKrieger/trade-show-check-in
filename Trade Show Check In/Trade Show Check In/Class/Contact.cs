﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trade_Show_Check_In.Class
{
    public class Contact
    {
        public string Name;
        public string Email;

        public string FirstLine;
        public string SecondLine;
        public string City;
        public string State;
        public string Zip;

        public string Note;

        public string TextFriendly()
        {
            string full = string.Empty;
            
            full = Name + Environment.NewLine;
            full += Email + Environment.NewLine;
            full += FirstLine + Environment.NewLine;
            if (String.IsNullOrWhiteSpace(SecondLine)) full += Email + Environment.NewLine;
            full += City + " " + State + " " + Zip + Environment.NewLine;
            if (String.IsNullOrWhiteSpace(Note)) full += Note + Environment.NewLine;

            return full;
        }
    }
}